# Data Science/ML

XYZ Learning Systems is building a learning solution which provides various modalities for individuals to learn. They have run a trial for a period of one month where a few candidates were given access to some of these modalities.

The data set `practice.csv` contains the activity details for 4 students.

## The flow

- A learning plan is created consisting of 3 goals.
- Each goal includes some tasks
- Each task consists of multiple modalities (av, gm, cb)
- Student is assessed intermittently through challenges or questions to see if they can recall what they learnt
- Each task can have multiple segments and levels.

## Assessment

- An assessment includes a cuestion with options
- User is expected to select an answerd from one or more options
- If response is correct then the status is marked as 'correct' else 'incorrect'
- If user does not reposnd within 15 seconds then the status is marked as 'timeout'
- For 'timeout'/ 'incorrect' responses the student can reattempt again up to 4 times
- If user responds corretly on first attempt then the 'first_time_right' is set to 1

## Objective

Perform explorative data analysis on the data set to see which factors affect learning and try to answer the following questions

- Does modality have any impact on the learning process?
- Which modality works best?
- Are students learning? What are the indicators that show that learning is successful
- Which students have made progress in their learning?
- Can we score/measure the progress?
- Which tasks are the ones where progress is more than others?
- Is one gender better than the other gender for a specific modality?

Use either Jupyter notebooks or R Markdown notebooks and share your EDA and results. When shareing the jupyter notebook, please ensure that the outputs are cleared before sending the file. It is recommended that you put your final code in a git repo like github or gitlab to share with us.
